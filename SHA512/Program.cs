﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using SHA512Convertor;
using System.Security.Cryptography;

WriteObjectInToExcelAsync(FileOpen.ShowDialog());
static void WriteObjectInToExcelAsync(string fileName)
{
    var workbook = new XLWorkbook(fileName);
    var worksheet = workbook.Worksheet(1);
    var count = worksheet.RowsUsed().Count();

    var outputWorkbook = new XLWorkbook();
    var newFileName = $"SHA512";
    var outputWorksheet = outputWorkbook.Worksheets.Add(newFileName);

    var rowCount = 1;
    for (int i = 1; i <= count; i++)
    {
        string originalString = worksheet.Cell(i, 1).GetString();

        if (originalString.StartsWith("0"))
        {
            originalString = "374" + originalString[1..];
        }

        if (originalString.StartsWith("+374"))
        {
            originalString = "374" + originalString[4..];
        }

        if (!originalString.StartsWith("374"))
            originalString = "374" + originalString;
        if (!string.IsNullOrEmpty(originalString))
        {
            SHA512 sha512 = SHA512.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(originalString);
            byte[] hashBytes = sha512.ComputeHash(inputBytes);
            string hashedString = BitConverter.ToString(hashBytes).Replace("-", "");

            outputWorksheet.Cell(i, 1).Value = hashedString;
            Console.WriteLine(rowCount);
            rowCount++;
        }
    }
    var outputFilePath = Path.GetDirectoryName(fileName) + "\\" + "SHA512_" + Path.GetFileNameWithoutExtension(fileName) + ".xlsx";
    outputWorkbook.SaveAs(outputFilePath);
}